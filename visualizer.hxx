//
// Created by UJJWAL on 07/09/2018.
//

#ifndef DETECTIONVISUALIZER_VISUALIZER_HXX
#define DETECTIONVISUALIZER_VISUALIZER_HXX

#include<iostream>
#include<string>
#include<vector>
#include<map>

typedef std::map<std::string, std::map<std::string, std::vector<unsigned
int>>> Detections;

Detections const * ReadDetectionFile(const std::string);


#endif //DETECTIONVISUALIZER_VISUALIZER_HXX
